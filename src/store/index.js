import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex);

import accessToken from './modules/access-token';
import user from './modules/user';

export default new Vuex.Store({
    modules : {
        accessToken : accessToken,
        user : user,
    }
});
