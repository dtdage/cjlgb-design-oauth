export default [
    {
        path : '/',
        name : 'index',
        meta :  { title : '首页', permission : '' },
        component : () => import(/* webpackChunkName: "views" */ "../../views/Index"),
    },{
        path : '/login',
        name : 'login',
        meta :  { title : '登录' },
        component : () => import(/* webpackChunkName: "views" */ "../../views/grant/Index"),
        props : (route) => ({
            callback : route.query.callback ? decodeURI(route.query.callback) : '/'
        })
    },{
        path : '/register',
        name : 'register',
        meta :  { title : '注册' },
        component : () => import(/* webpackChunkName: "views" */ "../../views/Register"),
        props : (route) => ({
            callback : route.query.callback ? decodeURI(route.query.callback) : '/'
        })
    },{
        path : '/authorize',
        name : 'authorize',
        meta :  { title : '授权', permission : '' },
        component : () => import(/* webpackChunkName: "views" */ "../../views/Authorize"),
        props : (route) => ({
            clientId : route.query['client_id'] ? route.query['client_id'] : undefined
        })
    },{
        path : '/logout',
        name : 'logout',
        meta :  { title : '退出' },
        component : () => import(/* webpackChunkName: "views" */ "../../views/Logout"),
        props : (route) => ({
            callback : route.query.callback ? decodeURI(route.query.callback) : '/'
        })
    }
];
